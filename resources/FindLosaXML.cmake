# - Try to find LosaXML
# Once done this will define
#
#  LosaXML_FOUND - system has LosaXML
#  LosaXML_INCLUDE_DIRS - the LosaXML include directories
#  LosaXML_LIBRARY_DIRS - the directories where the static libs of LosaXML were found
#  LosaXML_LIBRARIES -link these to use LosaXML
#  LosaXML_VERSION_STRING - the version of LosaXML found (since CMake 2.8.8)

#=============================================================================
# Copyright 2006-2012 Kitware, Inc.
# Copyright 2015 ViRuSTriNiTy <cradle-of-mail at gmx.de>
#
# Distributed under the OSI-approved BSD License (the "License");
# see accompanying file Copyright.txt for details.
#
# This software is distributed WITHOUT ANY WARRANTY; without even the
# implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the License for more information.
#=============================================================================
# (To distribute this file outside of CMake, substitute the full
#  License text for the above reference.)

set(LosaXML_INCLUDE_DIRS "$ENV{LosaXML_SOURCES}")
set(LosaXML_LIBRARY_DIRS "$ENV{LosaXML_LIB}")
set(LosaXML_LIBRARIES "LosaXML")

# handle the QUIETLY and REQUIRED arguments and set LosaXML_FOUND to TRUE if
# all listed variables are valid
include(${CMAKE_ROOT}/Modules/FindPackageHandleStandardArgs.cmake)
FIND_PACKAGE_HANDLE_STANDARD_ARGS(LosaXML
  FOUND_VAR LosaXML_FOUND
  REQUIRED_VARS LosaXML_INCLUDE_DIRS LosaXML_LIBRARY_DIRS
  VERSION_VAR LosaXML_VERSION_STRING)