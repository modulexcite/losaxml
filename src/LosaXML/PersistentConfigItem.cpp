// This file is part of the LosaXML library.
// Copyright (C) 2015 ViRuSTriNiTy <cradle-of-mail at gmx.de>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#include <LosaXML/PersistentConfigItem.h>
#include <LosaXML/DOMDocument.h>
#include <LosaXML/Utils.h> // for convertStr<>()
#include <boost/lexical_cast.hpp>

using namespace LosaXML;

CPersistentConfigItem::CPersistentConfigItem(CPersistentConfigItem * apParent, std::wstring aXmlNodeName)
  : CPersistentConfigItemNotificationListener()
  , mpParent(0)
  , mXmlNodeName(aXmlNodeName)
  , mNotificationListeners()
  , mRegisteredChildConfigs()
  , mFilename("")
  , mUpdateCount(0)
  , mUpdateNotifyChangedCalled(false)
{
  setParent(apParent);
}

CPersistentConfigItem::CPersistentConfigItem(const CPersistentConfigItem & aSource)
  : CPersistentConfigItemNotificationListener()
  , mpParent(0)
  , mXmlNodeName(aSource.mXmlNodeName)
  , mNotificationListeners()
  , mRegisteredChildConfigs()
  , mFilename(aSource.mFilename)
  , mUpdateCount(0)
  , mUpdateNotifyChangedCalled(false)
{
  setParent(aSource.mpParent);
}

CPersistentConfigItem::~CPersistentConfigItem()
{
  if (mNotificationListeners.size() > 0)
  {
    // first copy notification listener list because iterators might get invalidated due to notification
    // listener wants to remove itself from the notification listener list response to ConfigDeletionImminent()
    CNotificationListeners lNotificationListeners(mNotificationListeners);

    for (CPersistentConfigItemNotificationListener * i: lNotificationListeners)
      i->configDeletionImminent(this);
  }

  setParent(0);
}

void CPersistentConfigItem::beginUpdate()
{
  if (mUpdateCount == 0)
    mUpdateNotifyChangedCalled = false;

  ++mUpdateCount;
}

void CPersistentConfigItem::endUpdate()
{
  if (mUpdateCount > 0)
    --mUpdateCount;

  if (mUpdateCount == 0)
  {
    if (mUpdateNotifyChangedCalled)
      notifyChanged();
  }
}

void CPersistentConfigItem::notifyChanged()
{
  if (mUpdateCount == 0)
  {
    configChanged(this);

    for (CPersistentConfigItemNotificationListener * i: mNotificationListeners)
      i->configChanged(this);
  }
  else
    mUpdateNotifyChangedCalled = true;
}

void CPersistentConfigItem::configDeletionImminent(CPersistentConfigItem * apSender)
{
  mRegisteredChildConfigs.remove(apSender);
}

void CPersistentConfigItem::configChanged(CPersistentConfigItem * apSender)
{
  if (apSender != this)
    notifyChanged();
}

CPersistentConfigItem & CPersistentConfigItem::operator= (const CPersistentConfigItem & aSource)
{
  if (this == &aSource)
    return *this;

  mFilename = aSource.mFilename;

  return *this;
}

void CPersistentConfigItem::reset()
{
  beginUpdate();
  {
    for (CPersistentConfigItem * i: mRegisteredChildConfigs)
      i->reset();
  }
  endUpdate();
}

std::wstring CPersistentConfigItem::getXmlNodeName()
{
  return mXmlNodeName;
}

bool CPersistentConfigItem::writeToXml(CDOMNode & arConfigNode, std::wstring & arErrorMessage)
{
  if (write(arConfigNode, arErrorMessage))
  {
    return afterWriteToXml(arConfigNode, arErrorMessage);
  }
  else
    return false;
}

bool CPersistentConfigItem::canWriteToXml()
{
  return true;
}

bool CPersistentConfigItem::afterWriteToXml(CDOMNode & arConfigNode, std::wstring & arErrorMessage)
{
  for (CPersistentConfigItem * i: mRegisteredChildConfigs)
  {
    if (canWriteToXml())
    {
      CDOMNode lExistingConfigNode;
      if (arConfigNode.getNode(i->mXmlNodeName, lExistingConfigNode))
      {
        if (!i->writeToXml(lExistingConfigNode, arErrorMessage))
          return false;
      }
      else
      {
        CDOMNode lNewConfigNode(arConfigNode, i->mXmlNodeName);

        if (!i->writeToXml(lNewConfigNode, arErrorMessage))
          return false;
      }
    }
  }

  return true;
}

bool CPersistentConfigItem::beforeReadFromXml(CDOMNode & arConfigNode, bool aResetBeforeRead, std::wstring & arErrorMessage)
{
  if (aResetBeforeRead)
    reset();

  return true;
}

bool CPersistentConfigItem::readFromXml(CDOMNode & arConfigNode, bool aResetBeforeRead, std::wstring & arErrorMessage)
{
  if (beforeReadFromXml(arConfigNode, aResetBeforeRead, arErrorMessage))
  {
    if (read(arConfigNode, arErrorMessage))
    {
      return afterReadFromXml(arConfigNode, arErrorMessage);
    }
  }

  return false;
}

bool CPersistentConfigItem::afterReadFromXml(CDOMNode & arConfigNode, std::wstring & arErrorMessage)
{
  CDOMNode lConfigNode;

  for (CPersistentConfigItem * i: mRegisteredChildConfigs)
  {
    if (arConfigNode.getNode(i->mXmlNodeName, lConfigNode))
    {
      if (!i->readFromXml(lConfigNode, false, arErrorMessage))
      {
        return false;
      }
    }
  }

  return true;
}

bool CPersistentConfigItem::addNotificationListener(CPersistentConfigItemNotificationListener * apNotificationListener)
{
  if (apNotificationListener)
  {
    mNotificationListeners.push_back(apNotificationListener);

    return true;
  }
  else
    return false;
}

bool CPersistentConfigItem::removeNotificationListener(CPersistentConfigItemNotificationListener * apNotificationListener)
{
  auto i(mNotificationListeners.size());

  mNotificationListeners.remove(apNotificationListener);

  return i != mNotificationListeners.size();
}

bool CPersistentConfigItem::readFromFile(std::string aFilename, std::wstring & arErrorMessage) throw ()
{
  // first clear Config
  reset();

  // we called reset so it makes sense to take over filename immediatelly also
  mFilename = aFilename;

  // now read data from xml
  CDOMDocument p;

  try
  {
    CDOMRootNode lRootNode;
    if (p.read(aFilename, lRootNode, arErrorMessage))
    {
      if (lRootNode.getName().compare(mXmlNodeName) == 0)
      {
        if (readFromXml(lRootNode, false, arErrorMessage))
        {
          return true;
        }
      }
      else
        arErrorMessage = L"Root node name mismatch";
    }
  }
  catch (EGeneric &e)
  {
    arErrorMessage = LosaXML::convertString<std::wstring, std::string>(e.what());
  }
  catch (...)
  {
    arErrorMessage = L"Unknown error";
  }

  return false;
}

bool CPersistentConfigItem::saveToFile(std::string aFilename, std::wstring & arErrorMessage) throw ()
{
  if (canWriteToXml())
  {
    CDOMDocument p;

    try
    {
      CDOMRootNode lRootNode(p, mXmlNodeName);

      if (writeToXml(lRootNode, arErrorMessage))
      {
        if (p.write(aFilename, arErrorMessage))
        {
          mFilename = aFilename;

          return true;
        }
      }
    }
    catch (EGeneric &e)
    {
      arErrorMessage = LosaXML::convertString<std::wstring, std::string>(e.what());
    }
    catch (...)
    {
      arErrorMessage = L"Unknown error";
    }
  }

  return false;
}

const std::string & CPersistentConfigItem::getFilename() const
{
  return mFilename;
}

void CPersistentConfigItem::registerChildConfigItem(CPersistentConfigItem * apChildConfigItem)
{
  if (apChildConfigItem)
  {
    mRegisteredChildConfigs.push_back(apChildConfigItem);

    apChildConfigItem->addNotificationListener(this);
  }
}

void CPersistentConfigItem::unregisterChildConfigItem(CPersistentConfigItem * apChildConfigItem)
{
  if (apChildConfigItem)
  {
    apChildConfigItem->removeNotificationListener(this);

    mRegisteredChildConfigs.remove(apChildConfigItem);
  }
}

bool CPersistentConfigItem::read(CDOMNode & arConfigNode, std::wstring & arErrorMessage)
{
  return true;
}

bool CPersistentConfigItem::write(CDOMNode & arConfigNode, std::wstring & arErrorMessage)
{
  return true;
}

CPersistentConfigItem * CPersistentConfigItem::getParent()
{
  return mpParent;
}

void CPersistentConfigItem::setParent(CPersistentConfigItem * apValue)
{
  if (apValue != mpParent)
  {
    if (mpParent)
      mpParent->unregisterChildConfigItem(this);

    mpParent = apValue;

    if (mpParent)
      mpParent->registerChildConfigItem(this);
  }
}
