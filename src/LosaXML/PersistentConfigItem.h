// This file is part of the LosaXML library.
// Copyright (C) 2015 ViRuSTriNiTy <cradle-of-mail at gmx.de>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#ifndef LOSAXML_PERSISTENTCONFIGITEM_H_
#define LOSAXML_PERSISTENTCONFIGITEM_H_

#include <string>
#include <list>

namespace LosaXML
{
  class CDOMNode;
  class CPersistentConfigItem;

  class CPersistentConfigItemNotificationListener
  {
    protected:
      virtual void configDeletionImminent(CPersistentConfigItem * /*apSender*/) {};
      virtual void configChanged(CPersistentConfigItem * /*apSender*/) {};

    public:
      CPersistentConfigItemNotificationListener() {}
      virtual ~CPersistentConfigItemNotificationListener() {}

      friend class CPersistentConfigItem;
  };

  class CPersistentConfigItem : protected CPersistentConfigItemNotificationListener
  {
    typedef std::list<CPersistentConfigItemNotificationListener *> CNotificationListeners;
    typedef std::list<CPersistentConfigItem *> CRegisteredChildConfigs;

    private:
      CPersistentConfigItem * mpParent;
      std::wstring mXmlNodeName;
      CNotificationListeners mNotificationListeners;
      CRegisteredChildConfigs mRegisteredChildConfigs;
      std::string mFilename;
      unsigned int mUpdateCount;
      bool mUpdateNotifyChangedCalled;

      void registerChildConfigItem(CPersistentConfigItem * apChildConfigItem);
      void unregisterChildConfigItem(CPersistentConfigItem * apChildConfigItem);

    protected:
      virtual void configDeletionImminent(CPersistentConfigItem * apSender);
      virtual void configChanged(CPersistentConfigItem * apSender);

      virtual bool beforeReadFromXml(CDOMNode & arConfigNode, bool aResetBeforeRead, std::wstring & arErrorMessage);
      virtual bool afterReadFromXml(CDOMNode & arConfigNode, std::wstring & arErrorMessage);

      virtual bool afterWriteToXml(CDOMNode & arConfigNode, std::wstring & arErrorMessage);

      virtual bool read(CDOMNode & arConfigNode, std::wstring & arErrorMessage);
      virtual bool write(CDOMNode & arConfigNode, std::wstring & arErrorMessage);

    public:
      CPersistentConfigItem(CPersistentConfigItem * apParent, std::wstring aXmlNodeName);
      virtual ~CPersistentConfigItem();

      // copy constructor
      CPersistentConfigItem(const CPersistentConfigItem & aSource);

      // assignment operator
      CPersistentConfigItem & operator= (const CPersistentConfigItem & aSource);

      void beginUpdate();
      void endUpdate();
      void notifyChanged();

      virtual void reset();

      std::wstring getXmlNodeName();
      virtual bool canWriteToXml();
      bool writeToXml(CDOMNode & arConfigNode, std::wstring & arErrorMessage);
      bool readFromXml(CDOMNode & arConfigNode, bool aResetBeforeRead, std::wstring & arErrorMessage);

      virtual bool readFromFile(std::string aFilename, std::wstring & arErrorMessage) throw();
      virtual bool saveToFile(std::string aFilename, std::wstring & arErrorMessage) throw();

      const std::string & getFilename() const;

      CPersistentConfigItem * getParent();
      void setParent(CPersistentConfigItem * apValue);

      bool addNotificationListener(CPersistentConfigItemNotificationListener * apNotificationListener);
      bool removeNotificationListener(CPersistentConfigItemNotificationListener * apNotificationListener);
  };
}

#endif
