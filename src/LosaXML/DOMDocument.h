// This file is part of the LosaXML library.
// Copyright (C) 2015 ViRuSTriNiTy <cradle-of-mail at gmx.de>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#ifndef LOSAXML_DOMDOCUMENT_H_
#define LOSAXML_DOMDOCUMENT_H_

#include <string>
#include <vector>

namespace boost
{
  namespace property_tree
  {
    template <class A, class B, class C>
    class basic_ptree;

    namespace detail
    {
       template<class T>
       struct less_nocase;
    }
  }
}

namespace LosaXML
{
  // exceptions
  class EGeneric: public std::exception
  {
    typedef std::exception inherited;

    private:
      const char * mWhat;

    public:
      EGeneric(const char * aWhat) throw();
      virtual ~EGeneric() throw();

      const char* what() const throw();
  };

  class ERootNodeAlreadyPresent: public EGeneric
  {
    typedef EGeneric inherited;

    public:
      ERootNodeAlreadyPresent() throw();
      virtual ~ERootNodeAlreadyPresent() throw();
  };

  // parser classes
  class CDOMNode;
  class CDOMRootNode;
  class CDOMDocument;

  typedef std::vector<CDOMNode> CXMLNodeList;

  class CBaseDOMNode
  {
    friend class LosaXML::CDOMDocument;
    friend class LosaXML::CDOMRootNode;
    friend class LosaXML::CDOMNode;

    private:
      std::wstring mName;
      const CBaseDOMNode * mpParentNode;

      // note:
      // when adding new fields, keep in mind that the use of CXMLNodeList in
      // e.g. getNodes() involves shallow copying of object, therefore there
      //might be a need for a copy constructor

      template<typename T>
      bool getValue(T & arValue, std::wstring & arErrorMessage) const;

    protected:
      template<typename T>
      bool getNodeValue(std::wstring aNodeName, T aDefaultValue, T & arNodeValue, std::wstring & arErrorMessage) const;

      typedef boost::property_tree::basic_ptree<std::wstring, std::wstring, boost::property_tree::detail::less_nocase<std::wstring> > CBoostTreeNode;

      CBoostTreeNode * mpTreeNode;

      void checkUnderlyingTreeNode() const throw(EGeneric);

      void reinitializeXMLNode(std::wstring aName,
                               const CBaseDOMNode * mpParentNode,
                               CBoostTreeNode * apTreeNode);

    public:
      CBaseDOMNode();
      CBaseDOMNode(CBaseDOMNode & apParentNode, std::wstring aNodeName, std::wstring aNodeValue = L"") throw (EGeneric);
      virtual ~CBaseDOMNode();

      std::wstring getName() const;
      bool getValue(std::wstring & aValue, std::wstring & arErrorMessage) const;
      bool getValue(int & aValue, std::wstring & arErrorMessage) const;
  };

  class CXMLAttribute : public CBaseDOMNode
  {
    typedef CBaseDOMNode inherited;

    public:
      CXMLAttribute();
      CXMLAttribute(CDOMNode & apParentNode, std::wstring aAttributeName, std::wstring aAttributeValue = L"") throw (EGeneric);
      virtual ~CXMLAttribute();
  };

  class CDOMNode : public CBaseDOMNode
  {
    typedef CBaseDOMNode inherited;

    private:
      template <typename T>
      bool checkAttributeValuePrivate(const std::wstring & aAttributeName, const T & aValue) const;

      template <typename T>
      bool getAttributeValuePrivate(const std::wstring & aAttributeName, const T & aDefaultValue, T & arAttributeValue, std::wstring & arErrorMessage) const;

    public:
      CDOMNode();
      CDOMNode(CDOMNode & apParentNode, std::wstring aNodeName, std::wstring aNodeValue = L"") throw (EGeneric);
      virtual ~CDOMNode();

      void addNode(std::wstring aNodeName, std::wstring aNodeValue = L"");
      void addNode(std::wstring aNodeName, int aNodeValue = 0);
      void addNode(std::wstring aNodeName, unsigned int aNodeValue = 0);
      void addNode(std::wstring aNodeName, float aNodeValue = 0.0f);
      void addNode(std::wstring aNodeName, bool aNodeValue = false);
      void addNode(std::wstring aNodeName, wchar_t aNodeValue = 0);

      bool hasNode(std::wstring aNodeName) const;

      bool getNode(std::wstring aNodeName, CDOMNode & aNode, std::wstring * arErrorMessage = 0) const;

      bool getNodeValue(std::wstring aNodeName, std::wstring aDefaultValue, std::wstring & arNodeValue, std::wstring & arErrorMessage) const;
      bool getNodeValue(std::wstring aNodeName, int aDefaultValue, int & arNodeValue, std::wstring & arErrorMessage) const;
      bool getNodeValue(std::wstring aNodeName, unsigned int aDefaultValue, unsigned int & arNodeValue, std::wstring & arErrorMessage) const;
      bool getNodeValue(std::wstring aNodeName, float aDefaultValue, float & arNodeValue, std::wstring & arErrorMessage) const;
      bool getNodeValue(std::wstring aNodeName, bool aDefaultValue, bool & arNodeValue, std::wstring & arErrorMessage) const;
      bool getNodeValue(std::wstring aNodeName, wchar_t aDefaultValue, wchar_t & arNodeValue, std::wstring & arErrorMessage) const;

      bool getNodes(std::wstring aNodeName, CXMLNodeList & aNodeList) const;

      unsigned int getNodeCount(std::wstring aNodeName);

      bool hasAttribute(const std::wstring & aAttributeName) const;

      bool getAttribute(std::wstring aAttributeName, CXMLAttribute & aAttribute, std::wstring & arErrorMessage) const;

      bool getAttributeValue(const std::wstring & aAttributeName, const std::wstring & aDefaultValue, std::wstring & arAttributeValue, std::wstring & arErrorMessage) const;
      bool getAttributeValue(const std::wstring & aAttributeName, const int & aDefaultValue, int & arAttributeValue, std::wstring & arErrorMessage) const;
      bool getAttributeValue(const std::wstring & aAttributeName, const unsigned int & aDefaultValue, unsigned int & arAttributeValue, std::wstring & arErrorMessage) const;

      bool checkAttributeValue(const std::wstring & aAttributeName, const std::wstring & aValue) const;
      bool checkAttributeValue(const std::wstring & aAttributeName, const int & aValue) const;
  };

  class CDOMRootNode: public CDOMNode
  {
    typedef CDOMNode inherited;

    public:
      CDOMRootNode();
      CDOMRootNode(CDOMDocument & arDocument, std::wstring aNodeName) throw (ERootNodeAlreadyPresent);
  };

  class CDOMDocument
  {
    friend class LosaXML::CDOMNode;
    friend class LosaXML::CDOMRootNode;

    private:
      CDOMNode::CBoostTreeNode * mpXmlStructure;

    public:
      CDOMDocument();
      virtual ~CDOMDocument();

      bool read(std::string aXmlFilename, CDOMRootNode & arRootNode, std::wstring & rErrorMessage);
      bool write(std::string aXmlFilename, std::wstring & rErrorMessage);
  };
}

#endif
