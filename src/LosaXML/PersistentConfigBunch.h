// This file is part of the LosaXML library.
// Copyright (C) 2015 ViRuSTriNiTy <cradle-of-mail at gmx.de>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#ifndef LOSAXML_PERSISTENTCONFIGBUNCH_H_
#define LOSAXML_PERSISTENTCONFIGBUNCH_H_

#include <LosaXML/PersistentConfigItem.h>
#include <list>

namespace LosaXML
{
  template<class I>
  class CPersistentConfigBunchItemFactory
  {
    public:
      virtual ~CPersistentConfigBunchItemFactory()
      {}

      virtual I * createItem(CPersistentConfigItem * apParent)
      {
        return new I(apParent);
      }
  };

  template<class I>
  class CPersistentConfigBunch : public CPersistentConfigItem
  {
    typedef CPersistentConfigItem inherited;

    private:
      CPersistentConfigBunchItemFactory<I> & mItemFactory;
      std::list<I *> mItems;

    protected:
      void assign(const CPersistentConfigBunch<I> & aSource);

      virtual bool afterReadFromXml(CDOMNode & arConfigNode, std::wstring & arErrorMessage);
      virtual bool afterWriteToXml(CDOMNode & arConfigNode, std::wstring & arErrorMessage);

    public:
      CPersistentConfigBunch(CPersistentConfigItem * apParent, std::wstring aXmlNodeName,
        CPersistentConfigBunchItemFactory<I> & aItemFactory);
      virtual ~CPersistentConfigBunch();

      // copy constructor
      CPersistentConfigBunch(const CPersistentConfigBunch<I> & aSource);

      // assignment operator
      CPersistentConfigBunch<I> & operator= (const CPersistentConfigBunch<I> & aSource);

      virtual void reset();

      unsigned int size() const;
      bool get(typename std::list<I *>::const_iterator & arBeginIter, typename std::list<I *>::const_iterator & arEndIter) const;
      I * get(unsigned int aIndex) const;

      virtual bool contains(I * apItem) const;
      virtual bool extract(I * aprItem);

      // note: the following functions are not virtual because used in assign() which is called by copy ctor
      bool add(I * apItem, std::wstring & arErrorMessage);
      bool remove(I * & aprItem);
  };
}

#include <LosaXML/PersistentConfigBunch.cpp>

#endif
