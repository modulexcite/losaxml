@echo off
IF EXIST bin (
  rmdir /S /Q bin
)
IF EXIST CMakeFiles (
  rmdir /S /Q CMakeFiles
)
IF EXIST cmake_install.cmake del cmake_install.cmake /Q
IF EXIST CMakeCache.txt del CMakeCache.txt
IF EXIST Makefile del Makefile