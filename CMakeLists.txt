cmake_minimum_required(VERSION 2.8.12)
project(LosaXML)

find_package(Boost 1.57.0 REQUIRED)

# enable C++ 11
if (MINGW OR MSYS)
  add_compile_options(-std=c++0x)
endif()

set(CMAKE_ARCHIVE_OUTPUT_DIRECTORY bin)

include_directories(
  ${Boost_INCLUDE_DIRS}
  src/
)

set(SOURCES
  src/LosaXML/Utils.h
  src/LosaXML/DOMDocument.cpp
  src/LosaXML/PersistentConfigItem.cpp
  src/LosaXML/PersistentConfigBunch.cpp)

add_library(${PROJECT_NAME} STATIC ${SOURCES})